<?php

namespace Model;
require('PaymentMethodInterface.php');

class ShoppingCart
{
	/**
     * @var ArrayObject
     */
    protected $items;

    public function __construct()
    {
        $this->items = new \ArrayObject();
    }

    public function addItem(Item $item)
    {
        $this->items[] = $item;
        return $this;
    }

    public function calcTotalCost()
    {
        $total=0.0;
        foreach ($this->items as $item) {
            $total+=$item->getPrice();
        }
        return $total;
    }

    public function pay(PaymentMethodInterface $method)
    {
        $totalCost = $this->calcTotalCost();
        return $method->pay($totalCost);
    }    
      
}
