<?php

namespace Model;

class LoggerObserver implements \SplObserver
{
    public function update(\SplSubject $subject)
    {
        printf('Logger: Usuario %s creado a las %s'.PHP_EOL, $subject->getName(), date('H:i:s'));
        // file_put_contents('./registers.log', $subject->getName().'_'.date('H:i:s').PHP_EOL, FILE_APPEND | LOCK_EX);
    }
}