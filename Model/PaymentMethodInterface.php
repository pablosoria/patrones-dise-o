<?php

namespace Model;

interface PaymentMethodInterface
{

    /**
     * Pay amount
     *
     * @param float $amount
     *
     * @return boolean true if payment is successful
     */
    public function pay($amount);

}
