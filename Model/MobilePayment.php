<?php

namespace Model;

class MobilePayment implements PaymentMethodInterface
{
	/**
     * @var string
     */
    protected $serviceProvider;

	/**
     * @var string
     */
    protected $mobileNumber;

	/**
     * @var DateTime
     */
    protected $expires;

   public function __construct($serviceProvider, $mobileNumber, \DateTime $expires)
    {
        $this->serviceProvider = $serviceProvider;
        $this->mobileNumber = $mobileNumber;
        $this->expires = $expires;
    }

    public function getServiceProvider()
    {
        return $this->serviceProvider;
    }

    public function getMobileNumber()
    {
        return $this->mobileNumber;
    }

    public function getExpires()
    {
        return $this->expires;
    }

    public function pay($amount)
    {
    	return true; // if payment goes through
    }
}
