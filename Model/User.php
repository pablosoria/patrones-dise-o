<?php

namespace Model;

class User implements \SplSubject
{
    /**
     * @var SplObjectStorage
     */
    protected $observers;
    
    /**
     * @var string
     */    
    protected $name;
 
    public function __construct(\SplObjectStorage $observers)
    {
        $this->observers = $observers;
    }
 
    public function add($name)
    {
        $this->name = $name;
        $this->notify();
    }
 
    public function getName()
    {
        return $this->name;
    }
 
    public function attach(\SplObserver $observer)
    {
        $this->observers->attach($observer);
    }
 
    public function detach(\SplObserver $observer)
    {
        $this->observers->detach($observer);
    }
 
    public function notify()
    {
        foreach ($this->observers as $observer)
            $observer->update($this);
    }
}