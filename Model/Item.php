<?php

namespace Model;

class Item
{
	/**
     * @var string
     */
    protected $code;

	/**
     * @var string
     */
    protected $name;

	/**
     * @var float
     */
    protected $price;

   public function __construct($code, $name, $price)
    {
        $this->code = $code;
        $this->name = $name;
        $this->price = $price;
    }

    public function getCode()
    {
        return $this->code;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getPrice()
    {
        return $this->price;
    }

}
