<?php

namespace Model;

class MailObserver implements \SplObserver
{
    public function update(\SplSubject $subject)
    {
        echo 'Mail: Enviando email de bienvenida a '.$subject->getName().PHP_EOL;
        // mail('email@test.com', 'Bienvenido a Ofertix!', 'Mensaje de bienvenida');
    }
}