<?php

namespace Model;

class PayPal implements PaymentMethodInterface
{
	/**
     * @var string
     */
    protected $name;

	/**
     * @var string
     */
    protected $cardNumber;

	/**
     * @var DateTime
     */
    protected $expires;

   public function __construct($name, $cardNumber, \DateTime $expires)
    {
        $this->name = $name;
        $this->cardNumber = $cardNumber;
        $this->expires = $expires;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getCardNumber()
    {
        return $this->cardNumber;
    }

    public function getExpires()
    {
        return $this->expires;
    }

    public function pay($amount)
    {
    	return true; // if payment goes through
    }
}
