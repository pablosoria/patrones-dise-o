<?php

namespace Test;
require('../Model/ShoppingCart.php');
require('../Model/Item.php');
require('../Model/Visa.php');

use Model\ShoppingCart;
use Model\Item;
use Model\Visa;



class SamplePayProcessTest
{
	
    public function payBillUsingVisa()
    {
        $instance = new ShoppingCart();
        $a = new Item("XXXX1","T-Shirt", 14.95);
        $instance->addItem($a);

        $b = new Item("XXXX2","Shoes", 154.99);
        $instance->addItem($b);

        $expiryDate = $this->getCardExpiryDate();
        $visa = new Visa("Pablo Soria", "123456789", $expiryDate);
        $result = $instance->pay($visa);
        if ( $result == true ) {
            echo "Operation completed!";
        }

    }

    private function getCardExpiryDate()
    {
        $date = new \DateTime();
        $date->setDate(2016, 3, 1);
        return $date;
    }
  
      
}

$test = new SamplePayProcessTest();
$test->payBillUsingVisa();