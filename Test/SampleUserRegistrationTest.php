<?php

namespace Test;
require('../Model/User.php');
require('../Model/MailObserver.php');
require('../Model/LoggerObserver.php');


use Model\User;
use Model\MailObserver;
use Model\LoggerObserver;


class SampleUserRegistrationTest
{
	
    public function addUser($name)
    {
    	$user = new User(new \SplObjectStorage());
    	$user->attach(new MailObserver());
    	$user->attach(new LoggerObserver());
    	$user->add($name);
    }

}

$test = new SampleUserRegistrationTest();
$test->addUser('Jose Luis');
$test->addUser('Laia');
$test->addUser('Pablo');